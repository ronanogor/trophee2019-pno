standaloneAddContents({
  code: "pnc",
  title: "Défis programmation - Trophée Robotique 2019",
  folder: "2019/",
  tasks: [
    /* Niveau 1 : actions simples */
    { code: "e-1-0", title: "Avancer et rammasser" },

    { code: "e-1-1", title: "Le diamant" },

    { code: "e-1-2", title: "L'autre diamant" },

    { code: "e-1-3", title: "Les deux diamants" },

    { code: "e-1-4", title: "Comment sauter" },

    { code: "e-1-5", title: "Le diamant orange" },

    /* Niveau 2 : boucles simples */

    { code: "e-2-1", title: "Le champigon" },

    { code: "e-2-2", title: "Plein de champigons !" },

    { code: "e-2-3", title: "Aller au panneau" },

    { code: "e-2-4", title: "Goumo et sa clé" },

    /* Niveau 3 : boucles complexes */

    { code: "e-3-1", title: "Clés et serrures" },

    { code: "e-3-2", title: "Les plateformes" },

    { code: "e-3-3", title: "Champs de diamants" },

    /* Niveau 4 : conditions */

    { code: "e-4-1", title: "Le bloc SI" },

    { code: "e-4-2", title: "Les clés" },

    { code: "e-4-3", title: "Diamants à récupérer" }
  ]
});
