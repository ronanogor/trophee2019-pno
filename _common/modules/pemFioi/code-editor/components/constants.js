/*!
 * @author John Ropas
 * @since 17/12/2016
 */

CodeEditor.CONST = {
  LANGUAGES: {
    BLOCKLY: 'blockly',
    JAVASCRIPT: 'javascript',
    PYTHON: 'python'
  },
  SETTINGS: {
    DEFAULT_LANGUAGE: 'python',
    DEFAULT_LOCALIZATION: 'fr'
  },
  LOCALIZATION: {
    FR: 'fr',
    EN: 'en',
    DE: 'de'
  }
};

