/*!
 * @author John Ropas
 * @since 19/12/2016
 */

CodeEditor.Utils.Localization.Strings = {
  fr: {
    categories: {
      actions: "Actions",
      sensors: "Capteurs",
      debug: "Débuggage",
      colour: "Couleurs",
      dicts: "Dictionnaires",
      inputs: "Entrées",
      lists: "Listes",
      logic: "Logique",
      loops: "Boucles",
      math: "Maths",
      text: "Texte",
      variables: "Variables",
      functions: "Fonctions"
    },
    invalidContent: "Contenu invalide",
    unknownFileType: "Type de fichier non reconnu",
    download: "télécharger",
    smallestOfTwoNumbers: "Plus petit des deux nombres",
    greatestOfTwoNumbers: "Plus grand des deux nombres",
    programOfRobot: "Programme du robot",
    tooManyIterations: "Votre programme met trop de temps à se terminer !",
    submitProgram: "Valider le programme",
    runProgram: "Exécuter sur ce test",
    stopProgram: "|<",
    stepProgram: "|>",
    speed: "Vitesse :",
    slowSpeed: ">",
    mediumSpeed: ">>",
    fastSpeed: ">>>",
    ludicrousSpeed: ">|",
    selectLanguage: "Langage :",
    blocklyLanguage: "Blockly",
    javascriptLanguage: "Javascript",
    pythonLanguage: "Python",
    importFromBlockly: "Repartir de blockly",
    saveOrLoadProgram: "Enregistrer ou recharger votre programme :",
    avoidReloadingOtherTask:
      "Attention : ne rechargez pas le programme d'un autre sujet !",
    reloadProgram: "Recharger :",
    saveProgram: "Enregistrer",
    limitBlocks: "{remainingBlocks} blocs restants sur {maxBlocks} autorisés.",
    limitBlocksOver:
      "{remainingBlocks} blocs en trop utilisés pour {maxBlocks} autorisés.",
    limitElements:
      "{remainingBlocks} éléments restants sur {maxBlocks} autorisés.",
    limitElementsOver:
      "{remainingBlocks} éléments en trop utilisés pour {maxBlocks} autorisés.",
    buttons: {
      next: "Next",
      previous: "Previous"
    }
  },
  en: {
    categories: {
      actions: "Actions",
      sensors: "Sensors",
      debug: "Debug",
      colour: "Colors",
      dicts: "Dictionnaries",
      inputs: "Inputs",
      lists: "Lists",
      logic: "Logic",
      loops: "Loops",
      math: "Math",
      text: "Text",
      variables: "Variables",
      functions: "Functions"
    },
    invalidContent: "Invalid content",
    unknownFileType: "Unrecognized file type",
    download: "download",
    smallestOfTwoNumbers: "Smallest of the two numbers",
    greatestOfTwoNumbers: "Greatest of the two numbers",
    programOfRobot: "Robot's program",
    tooManyIterations: "Too many iterations before an action!",
    submitProgram: "Validate this program",
    runProgram: "Run this program",
    stopProgram: "Stop",
    speed: "Speed:",
    slowSpeed: "Slow",
    mediumSpeed: "Medium",
    fastSpeed: "Fast",
    ludicrousSpeed: "Very fast",
    selectLanguage: "Language :",
    blocklyLanguage: "Blockly",
    javascriptLanguage: "Javascript",
    pythonLanguage: "Python",
    importFromBlockly: "Generate from blockly",
    saveOrLoadProgram: "Save or reload your code:",
    avoidReloadingOtherTask: "Warning: do not reload code for another task!",
    reloadProgram: "Reload:",
    saveProgram: "Save",
    limitBlocks:
      "{remainingBlocks} blocks remaining out of {maxBlocks} available.",
    limitBlocksOver:
      "{remainingBlocks} blocks over the limit of {maxBlocks} available.",
    limitElements:
      "{remainingBlocks} elements remaining out of {maxBlocks} available.",
    limitElementsOver:
      "{remainingBlocks} elements over the limit of {maxBlocks} available.",
    buttons: {
      next: "Suivant",
      previous: "Précédent"
    }
  },
  de: {
    categories: {
      actions: "Aktionen",
      sensors: "Sensoren",
      debug: "Debug",
      colour: "Farben",
      dicts: "Diktionär",
      inputs: "Eingaben",
      lists: "Listen",
      logic: "Logik",
      loops: "Schleifen",
      math: "Mathe",
      text: "Texte",
      variables: "Variablen",
      functions: "Funktionen"
    },
    invalidContent: "Ungültiger Inhalt",
    unknownFileType: "Ungültiger Datentyp",
    download: "Herunterladen",
    smallestOfTwoNumbers: "Kleinste von zwei Zahlen",
    greatestOfTwoNumbers: "Größte von zwei Zahlen",
    programOfRobot: "Programm des Roboters",
    tooManyIterations: "Zu viele Iterationen vor einer Aktion!",
    submitProgram: "Programm überprüfen lassen",
    runProgram: "Programm ausführen",
    stopProgram: "Stop",
    speed: "Ablaufgeschwindigkeit:",
    slowSpeed: "Langsam",
    mediumSpeed: "Mittel",
    fastSpeed: "Schnell",
    ludicrousSpeed: "Sehr schnell",
    selectLanguage: "Sprache:",
    blocklyLanguage: "Blockly",
    javascriptLanguage: "Javascript",
    importFromBlockly: "Generiere von Blockly-Blöcken",
    saveOrLoadProgram: "Speicher oder lade deinen Quelltext:",
    avoidReloadingOtherTask:
      "Warnung: Lade keinen Quelltext von einer anderen Aufgabe!",
    reloadProgram: "Laden:",
    saveProgram: "Speichern",
    limitBlocks: "Noch {remainingBlocks} von {maxBlocks} Blöcken verfügbar.",
    // TODO :: translate next three
    limitBlocksOver:
      "{remainingBlocks} blocks over the limit of {maxBlocks} available.",
    limitElements:
      "{remainingBlocks} elements remaining out of {maxBlocks} available.",
    limitElementsOver:
      "{remainingBlocks} elements over the limit of {maxBlocks} available."
  }
};
