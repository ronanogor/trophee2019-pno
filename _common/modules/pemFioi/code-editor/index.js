/*!
 * @author John Ropas
 * @since 17/12/2016
 */

// namespacing
var CodeEditor = {};
CodeEditor.Utils = {};
CodeEditor.Utils.Shared = {};
CodeEditor.Utils.Localization = {};
CodeEditor.Utils.DOM = {};
CodeEditor.Controllers = {};
CodeEditor.CONST = {};
CodeEditor.Interpreters = {};





